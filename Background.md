## Background

### Heating systems


A heating system usually consists of:
 - a reservoir of hot watter – a boiler
 - system of pipes and heat exchangers – radiators in old heating systems
 - a mixer to exchange water between the vessel and the system of pipes and radiator
 - a pump to let the water circulate within this system.

In conventional heating systems the water is heated by the burning of gaseous or liquid fuel, while e.g. heat-pumps are used in renewable engery systems. A basic system with three radiators is depicted below. 


![System with a reservoir for hot water, a mixer, a pump and three radiators. Graphic for reservoir is taken from openclipart.org and modified.](./Heating_choice.svg)