# energy-usage-analysis

Within this project, the energy usage of a house build in the 50's to 70's located in Hamburg/Germany during the heating period 22/23 
is analyzed to gather and provide impulses for further optimizations.


## Roadmap


## Contributing
If you have ideas on further topics for analysis or errrors/missinterpretation or spot things which are hard to understand, please open an issue.

## Authors and acknowledgment
Rolf Würdemann

## License
License: CC-BY 4.0

## Project status
Active

## Credits

- Inkscape
- Zettlr
- Openclipart.org